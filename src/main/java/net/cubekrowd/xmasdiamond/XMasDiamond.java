package net.cubekrowd.xmasdiamond;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

public class XMasDiamond extends JavaPlugin implements Listener {

    private List<String> players;
    private boolean isEnabled;
    private boolean isAuto;
    private String start;
    private String end;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        getServer().getPluginManager().registerEvents(this, this);
        reloadConfigCache();
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
            if (isAuto) autoMode();
        }, 20L, 60 * 20L); // @NOTE(traks) every minute should be good enough
    }

    public void reloadConfigCache() {
        players = this.getConfig().getStringList("users");
        if (this.getConfig().getString("enable").equals("auto")) {
            isEnabled = false;
            isAuto = true;
        } else {
            isEnabled = this.getConfig().getBoolean("enable");
        }
        start = this.getConfig().getString("start");
        end = this.getConfig().getString("end");
    }

    @Override
    public void onDisable() {
        saveConfig();
    }

    public void autoMode() { // Check what isEnabled should be
        var currentD = LocalDateTime.now();
        int year = currentD.getYear();
        var formatter = DateTimeFormatter.ofPattern("y/d/M H:m", Locale.ENGLISH);
        var startD = LocalDateTime.parse(year + "/" + start, formatter);
        var endD = LocalDateTime.parse(year + "/" + end, formatter);
        if (endD.isBefore(startD)) {
            endD = LocalDateTime.parse((year + 1) + "/" + end, formatter);
        }

        boolean toggleEnable = startD.isBefore(currentD) && currentD.isBefore(endD);

        if (toggleEnable != isEnabled) {
            isEnabled = toggleEnable;
            String msg = toggleEnable ? "enabled" : "disabled";
            getLogger().info("Automatically " + msg + " based on time: " + currentD);
        }
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (!label.equalsIgnoreCase("xmas")) {
            return true;
        }

        if (args.length == 0) {
            if (!isEnabled) {
                sender.sendMessage(ChatColor.RED + "Plugin is not enabled.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "You must be a player to execute this command!");
                sender.sendMessage(ChatColor.RED + "(maybe you're looking for /xmas enable?)");
                return true;
            }

            Player p = (Player) sender;
            if (players.contains(p.getUniqueId().toString())) {
                p.sendMessage(ChatColor.RED
                        + "You've already collected this year's diamond! Come back next year!");
                return true;
            }

            if (p.getInventory().firstEmpty() == -1) {
                p.sendMessage(ChatColor.RED
                        + "Please have an open inventory slot before executing this command!");
                return true;
            }

            ItemStack diamond = new ItemStack(Material.DIAMOND, 1);
            ItemMeta diaMeta = diamond.getItemMeta();
            diaMeta.setDisplayName(ChatColor.GREEN + "Merry Christmas " + ChatColor.RED
                    + "from CubeKrowd " + Calendar.getInstance().get(Calendar.YEAR) + "!");
            diamond.setItemMeta(diaMeta);
            p.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.DARK_AQUA + "Cube" + ChatColor.GOLD
                    + "Krowd" + ChatColor.DARK_GRAY + "] " + ChatColor.RED + "Merry "
                    + ChatColor.GREEN + "Christmas!");
            p.spawnParticle(Particle.VILLAGER_HAPPY, p.getLocation().getX(),
                    p.getLocation().getY() + 1, p.getLocation().getZ(), 20, .5, .5, .5);

            players.add(p.getUniqueId().toString());
            p.getInventory().addItem(diamond);
            this.getConfig().set("users", players);
            saveConfig();

        } else if (args.length >= 1) {

            if (!sender.hasPermission("xmas.admin")) {
                sender.sendMessage(ChatColor.RED + "You do not have access to that command.");
                return true;
            }

            String action = args[0].toLowerCase();
            boolean toEnable = false;

            if (action.equals("enable")) {
                toEnable = true;
            } else if (action.equals("disable")) {
                toEnable = false;
            } else if (action.equals("auto")) {
                if (isAuto) {
                    sender.sendMessage(ChatColor.RED + "xmasdiamond already on auto mode.");
                    return true;
                }
                isAuto = true;
                this.getConfig().set("enable", "auto");
                saveConfig();
                sender.sendMessage(ChatColor.GREEN + "xmasdiamond now on auto mode.");
                return true;
            } else if (action.equals("reload")) {
                reloadConfig();
                reloadConfigCache();
                sender.sendMessage(ChatColor.GREEN + "Successfully reloaded.");
                return true;
            } else {
                sender.sendMessage(ChatColor.RED + "/xmas enable|disable|auto|reload");
                return true;
            }

            if (isEnabled != toEnable || isAuto) {
                isEnabled = toEnable;
                isAuto = false;
                this.getConfig().set("enable", toEnable);
                saveConfig();
                sender.sendMessage(ChatColor.GREEN + "xmasdiamond now " + action + "d.");
            } else {
                sender.sendMessage(ChatColor.RED + "xmasdiamond already " + action + "d!");
            }
        }

        return true;
    }
}
